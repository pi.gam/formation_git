Gestion de configuration décentralisée avec Git
===============================================

## Objectifs du module
A l’issu du module, l’étudiant doit être capable de :

* gérer l'historique d'un projet personnel
* collaborer sur un projet par l'intermédiaire d'un gestionnaire de configuration
* utiliser plusieurs versions d'un même projet en parallèle
* intégrer un gestionnaire de configuration dans une méthode de gestion de projet

## Compétences visées

### Git en solo

* utiliser git pour historiser un projet
  - créer un dépôt git
  - inspecter l'état courant d'un dépôt git
  - bâtir un commit : choisir et décrire les modifications
* gérer plusieurs branches sur un dépôt git
* gérer la synchronisation avec un serveur distant

### Projet logiciel avec git

* gérer le cycle de vie d'un projet
  - production/développement/release
* utilisation conjointe de git avec un outil de gestion de projet comme github
* chercher dans l'historique la source d'une modification
* intégrer git dans d'autres outils

## Prérequis

* familiarité avec les outils en ligne de commande

## Contenus

- Présentation de la gestion de conf et son intérêt
    - Historique de modifications
    - Centralisée vs décentralisée
    - Les autres outils
- Les commandes de base de Git
  - en local
  - travailler en local, synchroniser sur le réseau
- Fonctionnalités avancée avec Git:
  - branches : création et fusion
  - rebase
- Choisir son workflow
  - exemples de workflow classiques
- Travailler avec une plate-forme web d'hébergement de code : Github / Gitlab
- Gestion de hooks

## Modalités de mise en oeuvre

Les manipulations seront faites en ligne de commande.
Les applications web Github ou Gitlab serviront pour démontrer l'intégration de git
dans la gestion d'un projet de développement logiciel.
