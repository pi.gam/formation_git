# La documentation git dont vous êtes le héros

suivre le plan de
https://codewords.recurse.com/issues/two/git-from-the-inside-out

But: réaliser soi même une documentation git
à chaque étape:

1. expliquer à l'oral
2. faire rédiger dans un fichier, dans le dépôt perso
3. faire lire la partie sur l'article
4. faire appliquer la partie article sur un dépôt séparé

Pour chaque étape, donner le nom du fichier à respecter.
Lors de la mise en collaboration, des conflits émergeront tout naturellement.

## créer un dépôt : 

git init


.git: le dépôt 

le reste : working copy, le répertoire de travail

config: nom, couleur

Aller plus loin: prompt bash/zsh 

## travailler :

modifier un fichier
modifier un autre fichier

état d'un fichier: git status

les différents critères

suivi par git ou pas : le .gitignore, la version dans le homedir
~/.gitignore: les fichiers que l'on ignore systématiquement: éditeur de texte

https://github.com/github/gitignore

modifié ou pas depuis le dernier commit

git diff : observer les différences


ajouté au staging area : on sélectionne les fichiers que l'on veut pour le commit



commit, aller & retour:

git commit, -m

annuler le dernier commit
changer le message

## Exploitation de l'historique

git log

git checkout commitSHA1 : change le répertoire de travail 

git diff entre la version actuelle et un commit passé

notation simplifiée pour les commits parents



## le modèle objet

mes transparents de cours avec les images

en TP:

réinterpréter les opérations de base

https://codewords.recurse.com/issues/two/git-from-the-inside-out

## branchement, la base

faire diverger l'historique
maintenir plusieurs version en parallèle
maintenir plusieurs choses en parallèle: la doc et le code par exemple

git branch 
git checkout -b
git checkout

branche, HEAD, remote : à comprendre

merge:

fast-forward
vrai merge, conflit
visualiser avec gitk
en détail dans:
https://codewords.recurse.com/issues/two/git-from-the-inside-out

## workflow distant

branche remote: référencer un autre dépôt
https://codewords.recurse.com/issues/two/git-from-the-inside-out

fetch/merge, puis pull

clone
push: bare repository, git clone … --bare: juste le .git, pas de working copy




sauvegarder son travail à distance

application : gérer son environnement de travail, ses fichier .
un répertoire dans HOME, des liens symboliques
un script d'installation

## github/gitlab

branche remote, avec url ssh/https
bare repository, avec des outils en plus 

présentation des outils

## travailler à plusieurs

workflow classiques: feature branch locale, master partagée
pull request sur github/gitlab

https://www.atlassian.com/git/tutorials/comparing-workflows/

centralised
feature-branch: lier à une issue github/gitlab, pull request
git worklow: http://nvie.com/posts/a-successful-git-branching-model/

* master branch: branche contenant les releases
* develop branch: la branche dans laquelle on fusionne les feature branch

contribuer à un projet ponctuellement:

forker, développer dans son coin, push sur son fork, pull request

## maintenir un historique lisible

git rebase VS git master:
https://www.atlassian.com/git/tutorials/merging-vs-rebasing

git rebase -i

typiquement : reécrire l'historique de sa feature branch avant de la partager,
que ce soit pour un pullrequest ou un push

## hooks

maintenir un site web à partir d'un dépôt git contenant du markdown
donner la commande pour convertir un fichier md en html
expliciter les liens interfichier
décider du fichier de base : readme.md -> index.html
générer un fichier spécial contenant le résultat de git log 
générer un fichier contenant la liste des pages

# les fichiers à maintenir

pour la partie pratique 

git en solo
git : remote branches, push
git: le contenu du .git, le modèle objet derrière git
markdown: mémo

# Biblio

https://guides.github.com/
